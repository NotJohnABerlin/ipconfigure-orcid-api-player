const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: ['./app/index'],
  // use source map for debugging
  devtool: 'eval-source-map',
  output: {
    publicPath: '/',
    filename: 'app.js'
  },
  resolve: {
    modules: ['node_modules'],
    alias: {
      'react-dom': '@hot-loader/react-dom'
    },
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.(j|t)s(x)?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            // ignore the babel.config.js file used only for tests
            babelrc: false,
            presets: [
              // transpile only for last 3 versions of browsers
              [
                '@babel/preset-env',
                {
                  targets: { browsers: 'last 3 version' },
                  debug: true,
                  useBuiltIns: false
                }
              ],
              // handle jsx
              '@babel/preset-react'
            ],
            plugins: [
              // we need to transform class props
              ['@babel/plugin-proposal-class-properties', { loose: true }],
              // for use with async await
              '@babel/plugin-transform-runtime',
              // allow export * as name from 'esmodule'
              '@babel/plugin-proposal-export-namespace-from',
              // for hot reloading of react
              'react-hot-loader/babel'
            ]
          }
        }
      },
      {
        test: /\.s[ac]ss$/,
        // compile sass to css -> individual files
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                sourceMap: false,
                precision: 8
              }
            }
          }
        ]
      },
      {
        test: /\.css$/i,
        // load css into own files
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    }),
    new HtmlWebpackPlugin({
      meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'
      },
      template: 'app/index.template.html'
    }),
    new webpack.NamedModulesPlugin()
  ]
};
