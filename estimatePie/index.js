/**
 * Returns a generator that yields an infinite series starting at 1
 * and increases by 2 infinitely
 * @return {Generator<number>}
 */
function* infiniteSeries() {
  // the starting number of the series
  let i = 1;
  while (true) {
    // yield current number of the series
    yield i;
    // move to the next number in the series
    i += 2;
  }
}

/**
 * Returns a generator that yields an estimation of pie
 * @param {number} maxIterations - The maximum number of the iterations the
 * @return {Generator<{estimation: number, error: number, iteration: number}>}
 */
function* estimatePie(maxIterations) {
  let iteration = 1;
  let sum = 0.0;
  for (const i of infiniteSeries()) {
    if (iteration > maxIterations) break;
    // 1 raised to anything is one thus the last part of the summation
    // is always 1 divided by the current number in the infinite series
    sum += Math.pow(-1, (i - 1) / 2) * (1 / i);
    // arctan(1) = pie / 4 -> 4 * arctan(1) = pie
    const estimation = 4 * sum;
    // the absolute value of the error is used here because
    // when the sign of the error is negative our estimation was less
    // and when the sign of the error is positive the estimation is over
    const error = Math.abs((estimation - Math.PI) / Math.PI);
    // yield the estimation of estimatePie at the current iteration
    yield { estimation, error, iteration: iteration++ };
  }
}

module.exports = {
  estimatePie,
  infiniteSeries
};
