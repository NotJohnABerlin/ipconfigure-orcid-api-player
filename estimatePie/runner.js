const { estimatePie } = require('./index');

for (const { estimation, error, iteration } of estimatePie(10000)) {
  console.log(
    `The estimation of pie at iteration ${iteration} = ${estimation}`
  );
  console.log(
    `The error of the estimation at iteration ${iteration} = ${error}`
  );
  console.log(`Math.PI = ${Math.PI}\n`);
}
