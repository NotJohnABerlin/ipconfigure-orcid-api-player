# IpConfigure JavaScript Developer Take Home - John Berlin

## Setup

First you must install the dependencies using yarn or npm.
- `yarn install`
- `npm install`

Once that has completed you are ready to go

## Project Structure

The source code for each part of this take home can be found in the following directories

- Orchid API Player:  `app` 
- Estimation of pie: `estimatePie` 
- Tests: `tests` 

## Frameworks used

Orchid API Player
- React & Redux for the UI and state management
- SASS for the CSS flavor

Testing
- Jest & Enzyme

Estimation of pie
- Plain Node.js JavaScript 

## NPM Scripts
- `build-app`: Creates the production build of the application and places it in the `dist` directory
- `dev-app`: Starts a local development server on `http://localhost:8080` with hot reloading enabled
- `estimate-pie`: Runs the estimation of pie script `estimatePie/runner.js`
- `format`: Runs prettier on the directories listed in `Project Structure`
- `lint`: Lints the project using eslint
- `test`: Runs the unit tests with coverage
- `view-built-app`: Starts a server that will serve the built application on `http://localhost:5000`
