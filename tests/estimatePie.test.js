import { infiniteSeries, estimatePie } from '../estimatePie';

describe('Estimation of Pie', () => {
  describe('infiniteSeries', () => {
    it('Should yield one as the first number', () => {
      const series = infiniteSeries();
      expect(series.next()).toStrictEqual({ done: false, value: 1 });
    });

    it('Should yield numbers that increase by two after the first number', () => {
      const series = infiniteSeries();
      const expected = { done: false, value: 1 };
      expect(series.next()).toStrictEqual(expected);
      expected.value += 2;
      expect(series.next()).toStrictEqual(expected);
      expected.value += 2;
      expect(series.next()).toStrictEqual(expected);
      expected.value += 2;
      expect(series.next()).toStrictEqual(expected);
    });
  });

  describe('estimatePie', () => {
    it('Should only yield maxIteration estimations', () => {
      const estimator = estimatePie(10);
      let iteration = 1;
      for (const estimateAtIteration of estimator) {
        expect(estimateAtIteration.iteration).toBe(iteration++);
      }
      expect(estimator.next().done).toBe(true);
      expect(iteration).toBe(11);
    });

    it('Should yield values for iteration, estimation, and error', () => {
      for (const estimateAtIteration of estimatePie(1)) {
        expect(estimateAtIteration.iteration).toBeDefined();
        expect(estimateAtIteration.estimation).toBeDefined();
        expect(estimateAtIteration.error).toBeDefined();
      }
    });
  });
});
