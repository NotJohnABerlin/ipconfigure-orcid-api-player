import React from 'react';
import { mount } from 'enzyme';
import CameraImage from '../../app/components/CameraImage';
import { makeCameraFrameURL } from '../../app/constants';

afterAll(() => {
  jest.clearAllTimers();
});

describe('CameraImage Component', () => {
  it('Should render the image tag with the correct src url', () => {
    const wrapper = mount(
      <CameraImage name="dummy" streamId={12345} sessionId={'6789'} />
    );
    const frameURL = `${makeCameraFrameURL(12345, '6789')}`;
    expect(wrapper.getDOMNode().src).toBe(frameURL);
    wrapper.unmount();
  });

  it('Should render the image tag with the correct alt text', () => {
    const wrapper = mount(
      <CameraImage name="dummy" streamId={12345} sessionId={'6789'} />
    );
    expect(wrapper.getDOMNode().alt).toBe('Frame for dummy');
    wrapper.unmount();
  });

  it('Should constrain the image width and height by 400', () => {
    const wrapper = mount(
      <CameraImage name="dummy" streamId={12345} sessionId={'6789'} />
    );
    const img = wrapper.getDOMNode();
    expect(img.width).toBe(400);
    expect(img.height).toBe(400);
    wrapper.unmount();
  });

  it('Should start the image refresh interval once mounted', () => {
    jest.useFakeTimers();
    const wrapper = mount(
      <CameraImage name="dummy" streamId={12345} sessionId={'6789'} />
    );
    expect(setInterval).toBeCalledTimes(1);
    expect(setInterval).toBeCalledWith(expect.any(Function), 5000);
    wrapper.unmount();
    jest.useRealTimers();
  });

  it('Should refresh the image after five seconds', () => {
    jest.useFakeTimers();
    const wrapper = mount(
      <CameraImage name="dummy" streamId={12345} sessionId={'6789'} />
    );
    jest.advanceTimersToNextTimer();
    const frameURL = `${makeCameraFrameURL(12345, '6789')}&update=1`;
    expect(wrapper.state('tick')).toBe(1);
    expect(wrapper.state('frameURL')).toBe(frameURL);
    expect(wrapper.getDOMNode().src).toBe(frameURL);
    wrapper.unmount();
    jest.useRealTimers();
  });

  it('Should clear the interval on unmount', () => {
    jest.useFakeTimers();
    const wrapper = mount(
      <CameraImage name="dummy" streamId={12345} sessionId={'6789'} />
    );
    wrapper.unmount();
    expect(clearInterval).toBeCalledTimes(1);
    jest.useRealTimers();
  });
});
