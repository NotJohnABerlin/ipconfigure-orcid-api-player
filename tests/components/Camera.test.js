import React from 'react';
import { shallow, render } from 'enzyme';
import Camera from '../../app/components/Camera';

describe('Camera Component', () => {
  it('Should set the props of the CameraImage component correctly', () => {
    const wrapper = shallow(
      <Camera sessionId="abc" name="test" streamId={123} />
    );
    expect(wrapper.find('CameraImage').props()).toStrictEqual({
      streamId: 123,
      name: 'test',
      sessionId: 'abc'
    });
  });

  it('Should display the name of the camera in the p tag', () => {
    const wrapper = shallow(
      <Camera sessionId="abc" name="test" streamId={123} />
    );
    expect(wrapper.find('p').text()).toBe('test');
  });

  it('Should set the class of the first child to "camera"', () => {
    const wrapper = render(
      <Camera sessionId="abc" name="test" streamId={123} />
    );
    expect(wrapper.first().attr('class')).toBe('camera');
  });

  it('Should set the class of the first child to "camera"', () => {
    const wrapper = render(
      <Camera sessionId="abc" name="test" streamId={123} />
    );
    expect(wrapper.find('p').attr('class')).toBe('camera-name');
  });
});
