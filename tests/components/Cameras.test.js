import React from 'react';
import { shallow } from 'enzyme';
import { Cameras } from '../../app/components/Cameras';
import Loading from '../../app/components/Loading';
import CameraGrid from '../../app/components/CameraGrid';

describe('Cameras Component', () => {
  it('Should call the prop function fetchCameras if the prop fetch is false', () => {
    const fetchFn = jest.fn();
    shallow(
      <Cameras
        cameras={[]}
        session={{ id: 123 }}
        fetched={false}
        fetchCameras={fetchFn}
      />
    );
    expect(fetchFn).toBeCalledTimes(1);
  });

  it('Should not call the prop function fetchCameras if the prop fetch is true', () => {
    const fetchFn = jest.fn();
    shallow(
      <Cameras
        cameras={[]}
        session={{ id: '123' }}
        fetched={true}
        fetchCameras={fetchFn}
      />
    );
    expect(fetchFn).toBeCalledTimes(0);
  });

  it('Should render the loading component if there are no cameras', () => {
    const fetchFn = jest.fn();
    const wrapper = shallow(
      <Cameras
        cameras={[]}
        session={{ id: '123' }}
        fetched={true}
        fetchCameras={fetchFn}
      />
    );
    expect(wrapper.find(Loading)).toBeTruthy();
  });

  it('Should render the loading CameraGrid component if there are cameras', () => {
    const fetchFn = jest.fn();
    const wrapper = shallow(
      <Cameras
        cameras={[{}]}
        session={{ id: '123' }}
        fetched={true}
        fetchCameras={fetchFn}
      />
    );
    expect(wrapper.find(CameraGrid)).toBeTruthy();
    wrapper.unmount();
  });
});
