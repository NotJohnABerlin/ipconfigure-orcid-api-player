import React from 'react';
import { mount } from 'enzyme';
import CameraGrid from '../../app/components/CameraGrid';
import Camera from '../../app/components/Camera';

const DummyCameras = [
  {
    name: 'c1',
    primaryStream: {
      id: 1
    }
  },
  {
    name: 'c2',
    primaryStream: {
      id: 2
    }
  },
  {
    name: 'c3',
    primaryStream: {
      id: 3
    }
  },
  {
    name: 'c4',
    primaryStream: {
      id: 4
    }
  }
];

describe('CameraGrid Component', () => {
  it('Should render the supplied list of cameras', () => {
    const wrapper = mount(
      <CameraGrid sessionId="abc" cameras={DummyCameras} />
    );
    expect(wrapper.find(Camera).length).toBe(4);
    wrapper.unmount();
  });

  it('Should set the key of each div.camera-grid-item to the name of the camera', () => {
    expect.assertions(4);
    const wrapper = mount(
      <CameraGrid sessionId="abc" cameras={DummyCameras} />
    );
    wrapper.find(Camera).forEach((cgi, idx) => {
      expect(cgi.key()).toBe(`c${idx + 1}`);
    });
    wrapper.unmount();
  });

  it('Should set the props of the Camera component correctly', () => {
    expect.assertions(12);
    const wrapper = mount(
      <CameraGrid sessionId="abc" cameras={DummyCameras} />
    );
    wrapper.find(Camera).forEach((cam, idx) => {
      const cameraNameStreamId = idx + 1;
      const cameraName = `c${cameraNameStreamId}`;
      expect(cam.prop('sessionId')).toBe('abc');
      expect(cam.prop('name')).toBe(cameraName);
      expect(cam.prop('streamId')).toBe(cameraNameStreamId);
    });
    wrapper.unmount();
  });
});
