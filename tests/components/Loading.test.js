import React from 'react';
import { render } from 'enzyme';
import Loading from '../../app/components/Loading';

describe('Loading Component', () => {
  it('Should render as its first child a div with class loading-container', () => {
    const wrapper = render(<Loading />);
    expect(wrapper.first().attr('class')).toBe('loading-container');
  });

  it('Should render the spinner component if there is no failed reason', () => {
    const wrapper = render(<Loading />);
    expect(wrapper.find('svg')).toBeDefined();
  });

  it('Should render the failed reason if there was one', () => {
    const wrapper = render(<Loading failedReason={'boom'} />);
    const h2 = wrapper.find('h2');
    expect(h2.text()).toBe('boom');
  });
});
