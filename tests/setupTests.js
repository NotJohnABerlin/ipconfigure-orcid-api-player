import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

// setup enzyme for usage with react 16
configure({ adapter: new Adapter() });
