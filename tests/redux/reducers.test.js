import camerasReducer from '../../app/reducers/cameras';
import {
  FETCHING_CAMERAS,
  FETCH_FAILED,
  GOT_CAMERAS
} from '../../app/actions/cameras';

describe('camera reducer', () => {
  it('Should return the current state on any action type not recognized', () => {
    const result = camerasReducer(undefined, { type: 'init' });
    expect(result.failedReason).toBe(null);
    expect(result.session).toBe(null);
    expect(result.fetched).toBe(false);
    expect(result.list).toHaveLength(0);
  });

  it('Should only set the failed reason for action FETCH_FAILED', () => {
    const result = camerasReducer(undefined, { type: 'init' });
    const next = camerasReducer(result, {
      type: FETCH_FAILED,
      payload: { reason: 'boo' }
    });
    expect(next.failedReason).toBe('boo');
    expect(result.session).toBe(null);
    expect(result.fetched).toBe(false);
    expect(result.list).toHaveLength(0);
  });

  it('Should only set fetched to true for action FETCHING_CAMERAS', () => {
    const result = camerasReducer(undefined, { type: 'init' });
    const next = camerasReducer(result, {
      type: FETCHING_CAMERAS
    });
    expect(next.failedReason).toBe(null);
    expect(next.session).toBe(null);
    expect(next.fetched).toBe(true);
    expect(next.list).toHaveLength(0);
  });

  it('Should only set the session and list of cameras for acton GOT_CAMERAS', () => {
    const result = camerasReducer(undefined, { type: 'init' });
    const next = camerasReducer(result, {
      type: GOT_CAMERAS,
      payload: {
        session: '1',
        cameras: 2
      }
    });
    expect(next.failedReason).toBe(null);
    expect(next.fetched).toBe(false);
    expect(next.session).toBe('1');
    expect(next.list).toBe(2);
  });
});
