import axios from 'axios';
import MockAxios from 'axios-mock-adapter';
import configureStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise';
import * as cameraActions from '../../app/actions/cameras';
import * as constants from '../../app/constants';

const middlewares = [thunkMiddleware, promiseMiddleware];
const mockStore = configureStore(middlewares);
const mockAxios = new MockAxios(axios);
const expectedUnamePwrd = {
  username: 'liveviewer',
  password: 'tpain'
};

describe('store', () => {
  afterEach(() => {
    mockAxios.reset();
  });

  it('Dispatching the fetchCameras action should result in two other actions being dispatched', async () => {
    mockAxios.onPost(constants.USER_SESSION_URL).reply(200, {
      id: 'abc'
    });
    mockAxios.onGet(`${constants.CAMERAS_URL}abc`).reply(200, {
      cameras: []
    });
    const store = mockStore({});
    await store.dispatch(cameraActions.fetchCameras());
    const actions = store.getActions();
    expect(actions).toHaveLength(2);
    expect(actions[0].type).toBe(cameraActions.FETCHING_CAMERAS);
    expect(actions[1].type).toBe(cameraActions.GOT_CAMERAS);
  });

  it('Dispatching the doCamerasFetch action should use the configured username and password', async () => {
    mockAxios.onPost(constants.USER_SESSION_URL, expectedUnamePwrd).reply(200, {
      id: 'abc'
    });
    mockAxios.onGet(`${constants.CAMERAS_URL}abc`).reply(200, {
      cameras: []
    });
    const store = mockStore({});
    await store.dispatch(cameraActions.doCameraFetch());
    expect(mockAxios.history.post[0].data).toBe(
      JSON.stringify(expectedUnamePwrd)
    );
  });

  it('Dispatching the doCamerasFetch action should if creation of the new user session fails for 401 indicate as such', async () => {
    mockAxios.onPost(constants.USER_SESSION_URL, expectedUnamePwrd).reply(401);
    mockAxios.onGet(`${constants.CAMERAS_URL}abc`).reply(200, {
      cameras: []
    });
    const store = mockStore({});
    await store.dispatch(cameraActions.doCameraFetch());
    const actions = store.getActions();
    expect(actions[0].type).toBe(cameraActions.FETCH_FAILED);
    expect(actions[0].payload.reason).toBe(
      'The current user is not authorized to view camera feeds'
    );
  });

  it('Dispatching the doCamerasFetch action should if creation of the new user session fails for a reason that is not 401 indicate as such', async () => {
    mockAxios.onPost(constants.USER_SESSION_URL, expectedUnamePwrd).reply(404);
    mockAxios.onGet(`${constants.CAMERAS_URL}abc`).reply(200, {
      cameras: []
    });
    const store = mockStore({});
    await store.dispatch(cameraActions.doCameraFetch());
    const actions = store.getActions();
    expect(actions[0].type).toBe(cameraActions.FETCH_FAILED);
    expect(actions[0].payload.reason).toBe(
      'Failed to create a new session for the current user'
    );
  });

  it('Dispatching the doCamerasFetch action should if retrieval of cameras fails for 401 indicate as such', async () => {
    mockAxios.onPost(constants.USER_SESSION_URL, expectedUnamePwrd).reply(200, {
      id: 'abc'
    });
    mockAxios.onGet(`${constants.CAMERAS_URL}abc`).reply(401);
    const store = mockStore({});
    await store.dispatch(cameraActions.doCameraFetch());
    const actions = store.getActions();
    expect(actions[0].type).toBe(cameraActions.FETCH_FAILED);
    expect(actions[0].payload.reason).toBe(
      'The current user is unauthorized to view camera feeds'
    );
  });

  it('Dispatching the doCamerasFetch action should if retrieval of cameras fails for a reason that is not 401 indicate as such', async () => {
    mockAxios.onPost(constants.USER_SESSION_URL, expectedUnamePwrd).reply(200, {
      id: 'abc'
    });
    mockAxios.onGet(`${constants.CAMERAS_URL}abc`).reply(404);
    const store = mockStore({});
    await store.dispatch(cameraActions.doCameraFetch());
    const actions = store.getActions();
    expect(actions[0].type).toBe(cameraActions.FETCH_FAILED);
    expect(actions[0].payload.reason).toBe(
      'Failed to fetch the list of cameras for the current user'
    );
  });
});
