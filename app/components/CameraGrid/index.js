import './style.scss';
import React from 'react';
import * as PropTypes from 'prop-types';
import Camera from '../Camera';

/**
 * A simple component for rendering the grid of camera images.
 *
 * Note: We set the key of each camera component in order to
 * allow React to do diffing of component arrays easily
 * @param cameras - The list of cameras to be displayed
 * @param sessionId - The session id for the user
 */
export default function CameraGrid({ cameras, sessionId }) {
  // for each camera render a camera component within the grid
  return (
    <div className="camera-grid">
      {cameras.map(camera => (
        <Camera
          key={camera.name}
          sessionId={sessionId}
          name={camera.name}
          streamId={camera.primaryStream.id}
        />
      ))}
    </div>
  );
}

CameraGrid.propTypes = {
  cameras: PropTypes.arrayOf(PropTypes.object).isRequired,
  sessionId: PropTypes.string.isRequired
};
