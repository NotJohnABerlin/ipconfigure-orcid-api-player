import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { makeCameraFrameURL } from '../../constants';

/**
 * This component is responsible for rendering the image tag that
 * shows a frame from a camera primary stream.
 */
export default class CameraImage extends Component {
  static propTypes = {
    streamId: PropTypes.number.isRequired,
    sessionId: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    // the id of the refresh interval
    this.refreshId = null;
    // this components state is simple, an increasing number used to
    // refresh the image and the current frame url for use in the rendered
    // image tag
    this.state = {
      frameURL: makeCameraFrameURL(this.props.streamId, this.props.sessionId),
      tick: 0
    };
  }

  /**
   * The setInterval callback used the update our state
   */
  refreshImage = () => {
    // its best practice to use a function to update a components state
    // this way React can schedule our updates in order
    this.setState(oldState => {
      const frameURL = makeCameraFrameURL(
        this.props.streamId,
        this.props.sessionId
      );
      const nextTick = oldState.tick + 1;
      // we need to change the url of the image using a "cache" busting
      // trick e.g. add a dummy query param that changes
      return { frameURL: `${frameURL}&update=${nextTick}`, tick: nextTick };
    });
  };

  componentDidMount() {
    // start the refresh interval once the component has be rendered in the dom
    this.refreshId = setInterval(this.refreshImage, 5000);
  }

  componentWillUnmount() {
    // clear the refresh interval once we are removed from dom
    if (this.refreshId) {
      clearInterval(this.refreshId);
    }
  }

  render() {
    return (
      <img
        className="camera-image"
        src={this.state.frameURL}
        alt={`Frame for ${this.props.name}`}
        width="400"
        height="400"
      />
    );
  }
}
