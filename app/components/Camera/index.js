import './style.scss';
import React from 'react';
import * as PropTypes from 'prop-types';
import CameraImage from '../CameraImage';

/**
 * A simple component for displaying a camera's frame and its name
 * @param streamId - The id of the camera's primary stream
 * @param name - The name of the camera
 * @param sessionId - The session id for the user
 */
export default function Camera({ streamId, name, sessionId }) {
  return (
    <div className="camera">
      <CameraImage streamId={streamId} name={name} sessionId={sessionId} />
      <p className="camera-name">{name}</p>
    </div>
  );
}

// Dev run time type checking definitions
Camera.propTypes = {
  streamId: PropTypes.number.isRequired,
  sessionId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};
