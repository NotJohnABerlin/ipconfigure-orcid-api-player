import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as PropTypes from 'prop-types';
import * as actions from '../../actions';
import CameraGrid from '../CameraGrid';
import Loading from '../Loading';

// maps the actions to be dispatched into
// functions included in the components props
const mapDispatchToProps = dispatch => ({
  fetchCameras() {
    dispatch(actions.fetchCameras());
  }
});

// maps the state we care about into component props
const mapStateToProps = state => ({
  cameras: state.cameras.list,
  session: state.cameras.session,
  fetched: state.cameras.fetched,
  failedReason: state.cameras.failedReason
});

/**
 * This component is responsible for initiating the
 * process to retrieve the camera's and then rendering
 * the results
 * Note: exported for direct testing
 */
export class Cameras extends Component {
  static propTypes = {
    cameras: PropTypes.arrayOf(PropTypes.object),
    session: PropTypes.object,
    fetched: PropTypes.bool,
    failedReason: PropTypes.string,
    fetchCameras: PropTypes.func
  };

  componentDidMount() {
    // if we have not fetched, initiate fetch
    // otherwise do not re-fetch
    if (!this.props.fetched) {
      this.props.fetchCameras();
    }
  }

  render() {
    // if we have cameras to display, display the grid
    if (this.props.cameras.length > 0) {
      return (
        <CameraGrid
          cameras={this.props.cameras}
          sessionId={this.props.session.id}
        />
      );
    }
    // otherwise display the loading indicator
    return <Loading failedReason={this.props.failedReason} />;
  }
}

// The component connected to our store
const ConnectedCameras = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cameras);

export default ConnectedCameras;
