import React from 'react';

/**
 * A very simple functional component for displaying
 * the loading spinner
 */
export default function Spinner() {
  return (
    <span className="spinner">
      <svg
        width="125"
        height="125"
        viewBox="0 0 30 30"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle
          fill="none"
          stroke="#000"
          cx="15"
          cy="15"
          r="14"
          style={{
            strokeWidth: 0.222222
          }}
        />
      </svg>
    </span>
  );
}
