import './style.scss';
import React from 'react';
import Spinner from './Spinner';

/**
 * A simple component for displaying to the user
 * that the application is loading and if loading
 * fails displays the reason for it failing
 */
export default function Loading({ failedReason }) {
  return (
    <div className="loading-container">
      <h1>Loading</h1>
      {failedReason && <h2 style={{ color: 'red' }}>{failedReason}</h2>}
      {!failedReason && <Spinner />}
    </div>
  );
}
