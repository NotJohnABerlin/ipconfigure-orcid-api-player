import axios from 'axios';
import * as constants from '../constants';

// These are our action types, they represent what action occurred
export const FETCHING_CAMERAS = Symbol('fetching-cameras');
export const GOT_CAMERAS = Symbol('fetch-cameras-success');
export const FETCH_FAILED = Symbol('fetch-cameras-failure');

/**
 * Performs the actual creation of the user session and the fetching the list of cameras.
 */
export async function doCameraFetch() {
  // create a new user session
  let sessionRes;
  try {
    sessionRes = await axios.post(constants.USER_SESSION_URL, {
      username: 'liveviewer',
      password: 'tpain'
    });
  } catch (error) {
    // if there was response and it states we are unauthorized to create a session
    // indicate as such otherwise we use a canned failed reason
    let failedReason;
    if (error.response && error.response.status === 401) {
      failedReason = 'The current user is not authorized to view camera feeds';
    } else {
      failedReason = 'Failed to create a new session for the current user';
    }
    return {
      type: FETCH_FAILED,
      payload: { reason: failedReason }
    };
  }

  // retrieve the list of camera's
  const session = sessionRes.data;
  let camerasRes;
  try {
    camerasRes = await axios.get(`${constants.CAMERAS_URL}${session.id}`);
  } catch (error) {
    // if there was response and it states we are unauthorized to get the list
    // of cameras indicate as such otherwise we use a canned failed reason
    let failedReason;
    if (error.response && error.response.status === 401) {
      failedReason = 'The current user is unauthorized to view camera feeds';
    } else {
      failedReason = 'Failed to fetch the list of cameras for the current user';
    }
    return {
      type: FETCH_FAILED,
      payload: { reason: failedReason }
    };
  }

  // we have created our session and got the list of cameras
  return {
    type: GOT_CAMERAS,
    payload: {
      session,
      cameras: camerasRes.data.cameras
    }
  };
}

/**
 * This is what is called a thunk.
 * It allows us to dispatch a single action
 * that can in turn dispatch other actions e.g.
 * one to many.
 * @returns {function(*): *}
 */
export function fetchCameras() {
  return dispatch => {
    // let ourselves know that we have started
    // the fetching and do not need to re-fetch
    dispatch({ type: FETCHING_CAMERAS });
    // now do the actual fetching
    // the promise middleware will
    // handle the promise we dispatch
    return dispatch(doCameraFetch());
  };
}
