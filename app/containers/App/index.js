import './style.scss';
import React from 'react';
import { hot } from 'react-hot-loader/root';
import ConnectedCameras from '../../components/Cameras';

/**
 * This functional component acts primary entry point for
 * our application
 */
function App() {
  return (
    <main role="main">
      <ConnectedCameras />
    </main>
  );
}

// who does not like hot reloading
// this will be a non-op in production
export default hot(App);
