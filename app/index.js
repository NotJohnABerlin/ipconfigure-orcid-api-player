import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './containers/App';
import createStore from './store';

// create the store
const store = createStore();

// render our application with the store provider as the root component
render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app-mount')
);
