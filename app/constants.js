/**
 * The base API url
 * @type {string}
 */
export const BASE_API_URL = 'https://orchid.ipconfigure.com';

/**
 * The url used to create a new user session
 * @type {string}
 */
export const USER_SESSION_URL = `${BASE_API_URL}/service/sessions/user`;

/**
 * The base url used to retrieve the list of cameras
 * @type {string}
 */
export const CAMERAS_URL = `${BASE_API_URL}/service/cameras?sid=`;

/**
 * Creates and returns a URL for fetching a frame for the specified cameras
 * @param {number} primaryStreamId - The id of the cameras primary stream
 * @param {string} sessionId - The session id for the authenticated user
 * @return {string} - The url for viewing a frame from a cameras primary stream
 */
export const makeCameraFrameURL = (primaryStreamId, sessionId) =>
  `${BASE_API_URL}/service/streams/${primaryStreamId}/frame?sid=${sessionId}&fallback=true&time=0`;
