import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import * as actionCreators from '../actions';
import rootReducer from '../reducers';
import middleWare from './middleware';

// we want to allow the redux devtools extension to be able to
// be aware of our action creators, hence the need to create
// a special composer
const composer = composeWithDevTools({
  actionCreators
});

/**
 * Returns a redux store for development
 */
export default function configureStore() {
  return createStore(rootReducer, {}, composer(middleWare));
}
