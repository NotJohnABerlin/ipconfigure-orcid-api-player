import { applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise';

/**
 * Return a composed function that will apply our middlewares
 * in order from left to right
 */
export default applyMiddleware(thunkMiddleware, promiseMiddleware);
