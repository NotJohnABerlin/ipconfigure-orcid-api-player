import { createStore } from 'redux';
import rootReducer from '../reducers';
import middleWare from './middleware';

/**
 * Returns a redux store for production.
 */
export default function configureStore() {
  return createStore(rootReducer, {}, middleWare);
}
