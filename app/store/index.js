// we rely on webpack's define plugin to determine which version of the store is used
module.exports =
  process.env.NODE_ENV === 'production' ? require('./prod') : require('./dev');
