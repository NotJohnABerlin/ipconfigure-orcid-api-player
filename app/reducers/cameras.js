import {
  FETCHING_CAMERAS,
  GOT_CAMERAS,
  FETCH_FAILED
} from '../actions/cameras';

/**
 * This reducer is responsible for managing the camera state including
 * the session. For this app this is the simplest way.
 */
export default function camerasReducer(
  state = { fetched: false, session: null, failedReason: null, list: [] },
  { type, payload }
) {
  // we want to treat our state as immutable, each update if there was one
  // creates a new state object with properties that are only updated
  // if there was a change to them. If there was not an update the existing
  // state object is returned
  switch (type) {
    case FETCH_FAILED:
      return { ...state, failedReason: payload.reason };
    case FETCHING_CAMERAS:
      return { ...state, fetched: true };
    case GOT_CAMERAS:
      return { ...state, session: payload.session, list: payload.cameras };
    default:
      return state;
  }
}
