import { combineReducers } from 'redux';
import cameras from './cameras';

/**
 * Creates our root reducer. The name of the reducer equals
 * the name of part of our total state
 */
export default combineReducers({ cameras });
