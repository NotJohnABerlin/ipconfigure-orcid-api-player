const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

module.exports = {
  // put ourselves in production mode for optimizations
  mode: 'production',
  entry: ['./app/index'],
  output: {
    // we want out assets to be rooted under the URL they are served from
    publicPath: './',
    filename: 'app.js',
    path: path.join(__dirname, 'dist')
  },
  resolve: {
    modules: ['node_modules'],
    alias: {
      'react-dom': '@hot-loader/react-dom'
    },
    extensions: ['.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.js(x)?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            // ignore the babel.config.js file used only for tests
            babelrc: false,
            presets: [
              // transpile only for last 3 versions of browsers
              [
                '@babel/preset-env',
                {
                  targets: { browsers: 'last 3 version' },
                  useBuiltIns: false
                }
              ],
              // handle jsx
              '@babel/preset-react'
            ],
            plugins: [
              // we need to transform class props
              ['@babel/plugin-proposal-class-properties', { loose: true }],
              // for use with async await
              '@babel/plugin-transform-runtime',
              // allow export * as name from 'esmodule'
              '@babel/plugin-proposal-export-namespace-from',
              // ensure we use the production version
              'react-hot-loader/babel',
              // remove development prop type checking
              [
                'transform-react-remove-prop-types',
                {
                  ignoreFilenames: ['node_modules']
                }
              ]
            ]
          }
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        // compile sass to css -> use the css loader -> extract each css part
        // into a single file
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                sourceMap: false,
                precision: 8
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [
    // let ourselves know we are in production
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    // create a single css file out of all sass/css files used by the project
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].[hash].css'
    }),
    // create the index.html file for viewing the project
    new HtmlWebpackPlugin({
      meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'
      },
      template: 'app/index.template.html'
    }),
    new webpack.NamedModulesPlugin()
  ]
};
