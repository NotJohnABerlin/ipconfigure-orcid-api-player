module.exports = {
  // piggy back on prettier for rules
  extends: ['plugin:prettier/recommended', 'prettier/react'],
  // use eslints babel parse
  parser: 'babel-eslint',
  // set the JS language level to max in order to be able to parse our code
  parserOptions: {
    ecmaVersion: 10
  },
  // allow both node and web envs since we have a mix of Node.js and web code
  env: {
    browser: true,
    node: true
  }
};
